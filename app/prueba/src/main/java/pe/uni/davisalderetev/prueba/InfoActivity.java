package pe.uni.davisalderetev.prueba;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;



import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;


import android.widget.Button;
import android.widget.ImageView;

import android.widget.TextView;

import java.util.ArrayList;

public class InfoActivity extends AppCompatActivity {
    ImageView imageViewInfo;
    TextView textViewInfo;
    Button buttonLink;
    ArrayList<String> url_s=new ArrayList<>();

    int img_link,index;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        imageViewInfo=findViewById(R.id.image_view_info);
        textViewInfo=findViewById(R.id.text_view_info);
        buttonLink=findViewById(R.id.button_link);
        Intent intent=getIntent();
        String title=intent.getStringExtra("TITLE");
        img_link=intent.getIntExtra("IMAGE",R.drawable.logo);
        getUrls();
        index=intent.getIntExtra("INDEX",0);
        String url=url_s.get(index);
        textViewInfo.setText(title);
        imageViewInfo.setImageResource(img_link);

        buttonLink.setOnClickListener(v -> {
            Uri uri= Uri.parse(url);
            Intent intent1 = new Intent(Intent.ACTION_VIEW,uri);
            AlertDialog.Builder builder=new AlertDialog.Builder(InfoActivity.this);
            builder.setTitle(R.string.msg_title);
            builder.setCancelable(false);
            builder.setMessage(String.format(getResources().getString(R.string.msg_format),title));
            builder.setPositiveButton("Si", (dialog, which) -> startActivity(intent1));
            builder.setNegativeButton("NO", (dialog, which) -> {

            });
            builder.create().show();


        });


    }





    private void getUrls(){
        url_s.add("https://webbtelescope.org/contents/media/images/2022/032/01G72VSFW756JW5SXWV1HYMQK4");
        url_s.add("https://webbtelescope.org/contents/media/images/2022/031/01G77PKB8NKR7S8Z6HBXMYATGJ");
        url_s.add("https://www.space.com/james-webb-space-telescope-first-jupiter-photos");
        url_s.add("https://webbtelescope.org/contents/media/images/2022/034/01G7DA5ADA2WDSK1JJPQ0PTG4A");
        url_s.add("https://webbtelescope.org/contents/media/images/2022/033/01G709QXZPFH83NZFAFP66WVCZ");
        url_s.add("https://webbtelescope.org/contents/media/images/2022/035/01G7HRMY93K0BCCBKCABAQH0V7");
        url_s.add("https://webbtelescope.org/contents/media/images/2022/035/01G7F33FYJY94B9H7FW1APV030");

    }
}