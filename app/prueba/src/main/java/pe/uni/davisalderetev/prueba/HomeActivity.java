package pe.uni.davisalderetev.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
//import android.media.MediaPlayer;
import android.os.Bundle;

import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text=new ArrayList<>();
    ArrayList<Integer> image=new ArrayList<>();

    //MediaPlayer music;
    TextView textViewTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
       // music=MediaPlayer.create(HomeActivity.this,R.raw.soundtrack);
        //music.start();

        textViewTitle=findViewById(R.id.title_prueba);

        gridView=findViewById(R.id.grid_view);
        fillArray();

        Intent intent1=getIntent();
        String tit=intent1.getStringExtra("USER");
        textViewTitle.setText(tit);

        GridAdapter gridAdapter=new GridAdapter(this,text,image);
        gridView.setAdapter(gridAdapter);
        Intent intent= new Intent(HomeActivity.this,InfoActivity.class);
        gridView.setOnItemClickListener((parent,view,position,id)->{

            intent.putExtra("TITLE",text.get(position));
            intent.putExtra("IMAGE",image.get(position));
            intent.putExtra("INDEX",position);
            startActivity(intent);

        });


    }


    private void fillArray(){
        text.add("Atmosphere");
        text.add("Cosmic");
        text.add("Jupiter");
        text.add("Stephan’s Quintet");
        text.add("Nebula");
        text.add("Web Spectra");
        text.add("Web Spectrum");


        image.add(R.drawable.atmospfhere);
        image.add(R.drawable.cosmic);
        image.add(R.drawable.jupiter);
        image.add(R.drawable.quintet);
        image.add(R.drawable.ring_p);
        image.add(R.drawable.spectra);
        image.add(R.drawable.spectrum);
    }

}