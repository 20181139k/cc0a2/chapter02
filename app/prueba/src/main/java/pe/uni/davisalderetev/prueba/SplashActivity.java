package pe.uni.davisalderetev.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import android.media.MediaPlayer;


@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {
    //WebView webView;
    ImageView imageViewS;
    TextView textViewS;

    Animation animationImage,animationText;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MediaPlayer music=MediaPlayer.create(SplashActivity.this,R.raw.intro1);

        imageViewS=findViewById(R.id.image_view_splash);
        textViewS=findViewById(R.id.text_view_splash);


        animationImage=AnimationUtils.loadAnimation(this,R.anim.image_animation);
        animationText=AnimationUtils.loadAnimation(this,R.anim.text_animation);

        imageViewS.setAnimation(animationImage);
        textViewS.setAnimation(animationText);

        new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished){
                music.start();
            }
            @Override
            public void onFinish(){
                Intent intent=new Intent(SplashActivity.this,PruebaActivity.class);
                startActivity(intent);
                finish();
                music.pause();
            }
        }.start();

        /*
        webView=findViewById(R.id.webview);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        webView.loadUrl("https://bestanimations.com/Nature/Water/lake/lake-nature-animated-gif-18.gif");

        */


    }


}