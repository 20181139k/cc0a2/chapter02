package pe.uni.davisalderetev.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;

import java.util.Objects;

public class PruebaActivity extends AppCompatActivity {
    Button buttonLogin;
    EditText editTextUser,editTextPass;
    ImageView imageViewQR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);
        buttonLogin=findViewById(R.id.button_login);
        editTextUser=findViewById(R.id.userValue);
        editTextPass=findViewById(R.id.passValue);

        imageViewQR=findViewById(R.id.button_img_qr);

        imageViewQR.setOnClickListener(v->{
            GmsBarcodeScannerOptions options= new GmsBarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                    .build();
            GmsBarcodeScanner scanner= GmsBarcodeScanning.getClient(getApplicationContext(),options);

            scanner.startScan()
                    .addOnSuccessListener(this::SuccessAuth)
                    .addOnCanceledListener(() -> Toast.makeText(getApplicationContext(),R.string.scan_cancelled,Toast.LENGTH_LONG).show())
                    .addOnFailureListener(e -> FailureScan((MlKitException) e));
        });

        buttonLogin.setOnClickListener(v ->{
            String user=editTextUser.getText().toString();
            String pass=editTextPass.getText().toString();
            Intent intent= new Intent(PruebaActivity.this,HomeActivity.class);
            Verification(intent,user,pass);

        });



    }

    private void Verification(Intent intent,String user,String pass){
        String msg;
        if(user.equals("davis.alderete.v@uni.pe") && pass.equals("12345")){
            msg= String.format(
                    getResources().getString(R.string.edit_text_msg),
                    user
            );
            Toast.makeText(PruebaActivity.this,msg,Toast.LENGTH_LONG).show();
            intent.putExtra("USER",user);
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(getApplicationContext(),R.string.error_auth,Toast.LENGTH_LONG).show();

        }
    }

    private void SuccessAuth(Barcode barcode){

        String m=barcode.getRawValue();
        m= Objects.requireNonNull(m).substring(10);
        String[] k=m.split(";");
        String[] pass=k[2].split(":");

        //user=k[0]
        //pass=k[2].split(":")
        //pass=pass[1]

        Intent intentQR= new Intent(PruebaActivity.this,HomeActivity.class);
        Verification(intentQR,k[0],pass[1]);

    }

    private void FailureScan(MlKitException e){

        switch (e.getErrorCode()){
            case MlKitException.CODE_SCANNER_CANCELLED:
                Toast.makeText(PruebaActivity.this,getString(R.string.error_cancelled),Toast.LENGTH_LONG).show();
            case MlKitException.UNKNOWN:
                Toast.makeText(PruebaActivity.this,getString(R.string.unknow_error),Toast.LENGTH_LONG).show();

            default:
                Toast.makeText(PruebaActivity.this,getString(R.string.default_error,e),Toast.LENGTH_LONG).show();


        }
    }
}