package pe.uni.davisalderetev.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<String> text=new ArrayList<>();
    ArrayList<Integer> image=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView=findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter=new GridAdapter(this,text,image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent,view,position,id)-> Toast.makeText(getApplicationContext(),text.get(position),Toast.LENGTH_LONG).show());

    }

    private void fillArray(){
        text.add("Bird");
        text.add("Tiger");
        text.add("Human");


        image.add(R.drawable.buho);
        image.add(R.drawable.tigger);
        image.add(R.drawable.human);

    }
}