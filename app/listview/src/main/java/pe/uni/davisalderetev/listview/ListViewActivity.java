package pe.uni.davisalderetev.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListViewActivity extends AppCompatActivity {
    ListView listView;
    String[] countries;
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView=findViewById(R.id.list_view);
        countries=getResources().getStringArray(R.array.countries);
        arrayAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,countries);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            String country=parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(),String.format(getResources().getString(R.string.toast_msg),country),Toast.LENGTH_LONG).show();

        });
    }
}