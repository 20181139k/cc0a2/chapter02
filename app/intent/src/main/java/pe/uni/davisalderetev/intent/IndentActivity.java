package pe.uni.davisalderetev.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import pe.uni.davisalderetev.indent.R;

public class IndentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
    }
}