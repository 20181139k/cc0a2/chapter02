package pe.uni.davisalderetev.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


public class SharedPreferenceActivity extends AppCompatActivity {

    EditText editTextName;
    EditText editTextMessage;
    Button button;
    CheckBox checkBox;
    int counter = 0;
    SharedPreferences sharedPreference;
    String name;
    String message;
    Boolean isChecked;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        editTextName = findViewById(R.id.edit_text_name);
        editTextMessage = findViewById(R.id.edit_text_message);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.check_box);

        button.setOnClickListener(v -> {
            counter++;
            button.setText(String.valueOf(counter));
        });

        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isChecked = checkBox.isChecked();

        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString("key name", name);
        editor.putString("key message", message);
        editor.putInt("key counter", counter);
        editor.putBoolean("key remember", isChecked);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG).show();
    }

    private void retrieveData() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = sharedPreference.getString("key name", null);
        message = sharedPreference.getString("key message", null);
        counter = sharedPreference.getInt("key counter", 0);
        isChecked = sharedPreference.getBoolean("key remember", false);

        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isChecked);
    }
}